<?php

namespace App\Http\Controllers;

use App\Author;
use App\Book;
use App\Category;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_books = Book::all();
        return view('admin.books.index')->with('all_posts',$all_books);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();

        if ($category->count() == 0) {
            session()->flash('info', 'you must have one category');
            return redirect()->back();
        }
        $author = Author::all();

        if ($author->count() == 0) {
            session()->flash('info', 'you must have one Author');
            return redirect()->back();
        }


        return view('admin.books.create', compact('category'))->with('authors', Author::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required',
            'ISN'=>'required|unique:books',
            'featured' => 'required|image',
            'number' => 'required',
            'category_id' => 'required',
            'author_id' => 'required'
        ],[],[
            'tags'=>'author'
        ]);

        $featured = $request->featured;
        $newFeatured = time() . $featured->getClientOriginalName();

        $featured->move('uploads', $newFeatured);

        $post = Book::create([
            'name' => $request->title,
            'ISN'=>$request->ISN,
            'book_cover' =>$newFeatured,
            'number' => $request->number,
            'category_id' => $request->category_id,
            'author_id'=>$request->author_id,
        ]);

        session()->flash('success', 'Book created successful');

        return redirect()->route('books.index');


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::find($id);
        $category = Category::all();
        return view('admin.books.edit', ['book' => $book, 'category' => $category])->with('authors', Author::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'ISN'=>'required',
            'number' => 'required',
            'category_id' => 'required',
            'author_id' => 'required'
        ],[],[
            'tags'=>'author'

        ]);

        $post = Book::find($id);

        if ($request->hasFile('featured')) {
            $featured = $request->featured;
            $feturead_new_name = time() . $featured->getClientOriginalName();
            $featured->move('uploads', $feturead_new_name);
            $post->book_cover = $feturead_new_name;
        }

        $post->update([
            'name' => $request->title,
            'ISN'=>$request->ISN,
            'number' => $request->number,
            'category_id' => $request->category_id,
            'author_id'=>$request->author_id,
        ]);

        session()->flash('success', 'Book update success');
        return redirect()->route('books.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Book::find($id)->delete();
        session()->flash('success', 'Book trashed successfuly');
        return back();
    }

    public function trashed()
    {
        $trashed = Book::onlyTrashed()->get();

        return view('admin.books.trashed', ['trahsed' => $trashed]);

    }

    public function kill($id)
    {
        $trashed = Book::withTrashed()->where('id', $id)->first();
        $trashed->forceDelete();

        session()->flash('success', 'Books Deleted successfully');
        return back();

    }

    public function restore($id)
    {
        $restore = Book::withTrashed()->where('id', $id)->first();

        $restore->restore();
        session()->flash('success', 'Book restored successfully');

        return redirect()->route('books.trashed');
    }


}
