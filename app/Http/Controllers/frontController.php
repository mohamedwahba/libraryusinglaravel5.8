<?php

namespace App\Http\Controllers;

use App\Author;
use App\Book;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class frontController extends Controller
{
    public function singlePage($id)
    {
        $category = Category::find($id);
        $book = $category->books->all();

        return view('single')
            ->with('my_category', $category)
            ->with('books', $book)
            ->with('category', Category::take(5)->get());


    }

    public function borrow($id)
    {

        $book = Book::find($id);
        $auth = Auth::user()->id;

        $row = DB::table('book_user')
            ->where('book_id', '=', $book->id)
            ->where('user_id', '=', $auth)
            ->first();
        if ($row) {
            session()->flash('info', 'Your have request this book before');
            return redirect()->back();

        } else {

            $book->users()->attach($auth);
            session()->flash('success', 'Your Request Send to admin Please wait Response');
            return redirect()->back();
        }
    }

    public function listBorrowBook()
    {
        $all_books = [];
        $user = Auth::user()->id;
        $row = DB::table('book_user')
            ->where('user_id', '=', $user)
            ->where('returned', '=', 0)
            ->get()->toArray();

        foreach ($row as $book)
        {
           $books = Book::find($book->book_id);
           array_push($all_books,$books);
        }


        return view('list_Book')
            ->with('category', Category::take(5)->get())
            ->with('books',$all_books)
            ->with('row',$row)
            ;

    }

    public function cancel($id)
    {

        $book = Book::find($id);
        $auth = Auth::user()->id;

        $book->users()->updateExistingPivot($auth, array('returned' => 1));


        $book->number+=1;
        $book->save();

//        $book->users()->detach($auth);
        session()->flash('success', 'Your Book Returned');
        return redirect()->back();


    }


}
