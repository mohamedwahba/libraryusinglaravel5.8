<?php

namespace App\Http\Controllers;

use App\Book;
use App\Category;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BorrowRequestController extends Controller
{
    public function borrow()
    {
        $all_books = [];
        $all_status = [];
        $all_users = [];
        $returned = [];
        $row = DB::table('book_user')
            ->get()->toArray();


        foreach ($row as $book)
        {
            $books = Book::find($book->book_id);
            array_push($all_books,$books);
        }

        foreach ($row as $users)
        {
            $user = User::find($users->user_id);
            array_push($all_users,$user);
        }


        foreach ($row as $ret)
        {
            array_push($returned,$ret->returned);
        }


        foreach ($row as $status)
        {
            array_push($all_status,$status->status);
        }

        $data = [];
        for ($i=0;$i<= count($all_books)-1 ; $i++){
            array_push($data,[
                'book'=> $all_books[$i],
                'status' => $all_status[$i],
                'users' =>$all_users[$i],
                'return' =>$returned[$i],
            ]);
        }

        return view('admin.borrow')
            ->with('category', Category::take(5)->get())
            ->with('data',$data)
            ;

    }

    public function accept($id)
    {
        $row = DB::table('book_user')
            ->where('book_id', '=', $id)
            ->first();
        $book = Book::find($row->book_id);
        if ( $book->number > 0)
        {
            $book->number -=1;
            $book->save();
            $status = 'Accepted';
            $book->users()->updateExistingPivot($row->user_id, array('status' => $status));
            session()->flash('success','Accepted ');

        }else{
            $book->status ='unavailable';
            $book->save();
            session()->flash('info','No More Books In Repo');
        }
        return redirect()->back();
    }

    public function admin_cancel($id)
    {
        $users = [];
        $book = Book::find($id);
        $row = DB::table('book_user')
            ->get()->toArray();

        foreach ($row as $user)
        {
            array_push($users,$user->user_id);
        }

        $book->number+=1;
        $book->save();

        $book->users()->detach($users);
        session()->flash('success', 'Your Book Returned');
        return redirect()->back();


    }
}
