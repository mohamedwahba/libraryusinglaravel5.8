<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function search($str)
    {
        $books = [];
        $row = DB::table('books')
            ->where('name', 'like', '%'.$str.'%')
            ->get();

        foreach ($row as $name)
        {
            array_push($books,$name->name);
        }

        return response()->json($books,200);
    }
}
