<?php

namespace App\Http\Controllers;

use App\Book;
use App\Category;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function about()
    {
        return view('about');
    }


    public function contact()
    {
        return view('contact');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home')
            ->with('category', Category::take(5)->get())
            ->with('books',Book::paginate(9))

            ;
    }

    public function admin()
    {
        return view('admin.index')
            ->with('books_count',Book::all()->count())
            ->with('user_count',User::all()->count())
            ->with('category_count',Category::all()->count())
            ;
    }

    public function home()
    {
        return view('welcome')
            ->with('category', Category::take(5)->get())
            ->with('books',Book::paginate(9));

    }
}
