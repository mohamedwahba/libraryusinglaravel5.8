<?php

use Illuminate\Database\Seeder;

class userTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('123456789'),
            'role' => 1,
            'phone'=>'01066243716',
        ]);

        \App\profile::create([
            'user_id' =>$user->id,
            'avatar' => 'avatars/1.jpg',
            'about' =>'test test test ',
            'facebook'=>'https://www.facebook.com/',
            'youtube'=>'https://www.facebook.com/',
        ]);
    }
}
