<?php

use Illuminate\Database\Seeder;

class bookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(\App\Author::class,3)->create();
        factory(\App\Category::class,3)->create();
        factory(\App\Book::class,10)->create();
    }
}
