<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/view/category/{id}','frontController@singlePage')->name('category.front')->middleware('auth');

Route::get('borrow/book/{id}','frontController@borrow')->name('borrow')->middleware('auth');
Route::get('list/user/borrow','frontController@listBorrowBook')->name('my_borrow_book')->middleware('auth');
Route::get('/cancel/borrow/{id}','frontController@cancel')->name('borrow.cancel')->middleware('auth');

Route::get('/about','HomeController@about')->name('about');
Route::get('/contact','HomeController@contact')->name('contact');


Route::get('/live/search/{str}','SearchController@search');

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::resource('category', 'categoriesController');
    Route::resource('author', 'AuthorContoller');
    Route::get('/admin', 'HomeController@admin')->name('admin');

    Route::resource('/users', 'UsersController');
    Route::get('/users/admin/{id}', 'UsersController@makeAdmin')->name('users.admin');
    Route::get('/users/admin/user/{id}', 'UsersController@makeNotAdmin')->name('users.user');
    Route::resource('/profiles/change', 'profileController');

    Route::get('/request','BorrowRequestController@borrow')->name('book.request');
    Route::get('/accept/{id}','BorrowRequestController@accept')->name('borrow.accept');
    Route::get('/admin/cancel/{id}','BorrowRequestController@admin_cancel')->name('admin.cancel');



    Route::resource('books', 'BookController');
    Route::get('/trashed', 'BookController@trashed')->name('books.trashed');
    Route::delete('/kill/{id}', 'BookController@kill')->name('books.kill');
    Route::get('/restore/{id}', 'BookController@restore')->name('books.restore');



});
