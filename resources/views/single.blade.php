@extends('layouts.front')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card align-center">
                    <div class="card-header">{{$my_category->name}}</div>
                    @if(session()->has('success'))
                        <p class="alert alert-success">{{session()->get('success')}}</p>
                        @endif

                    @if(session()->has('info'))
                        <p class="alert alert-danger">{{session()->get('info')}}</p>
                    @endif

                    <div class="card-body">

                        <div class="row m-auto">
                            @foreach($books as $book)
                                <div class="card m-3 p-2" style="width: 18rem;">
                                    <img class="card-img-top" src="{{asset('uploads/'.$book->book_cover)}}"
                                         alt="Card image cap" height="200px">
                                    <div class="card-body">
                                        <h5 class="card-title">BookName: {{$book->name}}</h5>
                                        <p class="card-text">Author: {{$book->author->name}}</p>
                                        @if($book->status == 'unavailable')
                                        <p class="card-text text-center alert alert-danger">{{$book->status}}</p>
                                        @else
                                            <p class="card-text text-center alert alert-success">{{$book->status}}</p>

                                        @endif

                                        <a href="{{route('borrow',['id'=>$book->id])}}" class="btn btn-primary form-control">Borrow</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>




                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
