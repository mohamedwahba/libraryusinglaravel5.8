@extends('layouts.adminLayout')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="row col-12">
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-header">
                                        Number of Books
                                    </div>
                                    <div class="card-body">
                                        <h1 class="text-center"> {{$books_count}}</h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-header">
                                        Number of Category
                                    </div>
                                    <div class="card-body">
                                        <h1 class="text-center"> {{$category_count}}</h1>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-header">
                                        Number of Users
                                    </div>
                                    <div class="card-body">
                                        <h1 class="text-center"> {{$user_count}}</h1>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
