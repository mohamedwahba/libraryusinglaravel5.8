@extends('layouts.adminLayout')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card align-center">
                    @if(session()->has('info'))
                        <p class="alert alert-success">{{session()->get('info')}}</p>
                    @endif

                    <table class="table table-striped">
                        @if(sizeof($data) > 0)

                            <thead>
                            <tr>
                                <th scope="col">ISN</th>
                                <th scope="col">Name</th>
                                <th scope="col">User</th>
                                <th scope="col">Returned</th>
                                <th scope="col">Cpoies Available</th>
                                <th scope="col">Status</th>
                                <th scope="col">Accept</th>
                                <th scope="col">Reject</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $obj)
                                <tr>
                                    <td>{{$obj['book']->ISN}}</td>
                                    <td>{{$obj['book']->name}}</td>
                                    <td>{{$obj['users']->name}}</td>
                                    @if($obj['return'] == 1)
                                        <td>Returned</td>
                                    @else
                                        <td>borrow</td>

                                    @endif
                                    <td>{{$obj['book']->number}}</td>

                                    @if($obj['status'] == 'Accepted')
                                        <td class="alert alert-success">{{$obj['status']}}</td>
                                    @else
                                        <td class="alert alert-warning">{{$obj['status']}}</td>

                                    @endif
                                    <td>
                                        @if($obj['status'] == 'pending')
                                            <a href="{{route('borrow.accept',['id'=>$obj['book']->id])}}"
                                               class="btn btn-success">
                                                Accept
                                            </a>
                                    @endif
                                    <td><a href="{{route('admin.cancel',['id'=>$obj['book']->id])}}"
                                           class="btn btn-danger">Reject</a>
                                    </td>
                                </tr>

                            @endforeach
                            </tbody>
                        @else
                            <tr>
                                <th colspan="5" class="text-center">No Books</th>
                            </tr>
                        @endif
                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection
