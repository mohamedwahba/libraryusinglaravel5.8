@extends('layouts.adminLayout')
@section('content')


    <div class="card">

        <div class="card-body">
            <table class="table table-hover">
                <tr>
                    <th>cover</th>
                    <th>name</th>
                    <th>available copies</th>
                    <th>Status</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                <tbody>
                @if($all_posts->count() > 0)

                    @foreach($all_posts as $post)
                        <tr>
                            <td><img src="{{asset('uploads/'.$post->book_cover)}}" width="80px" height="80px"></td>
                            <td>{{$post->name}}</td>
                            <td>{{$post->number}}</td>
                            <td>{{$post->status}}</td>
                            <td><a href="{{route('books.edit',['id'=>$post->id])}}" class="btn btn-success">Edit</a></td>
                            <td>
                                <form action="{{route('books.destroy',['id'=>$post->id])}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="Delete" class="btn btn-danger">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <th colspan="5" class="text-center">No Books</th>
                    </tr>
                @endif


                </tbody>
            </table>
        </div>
    </div>



@endsection