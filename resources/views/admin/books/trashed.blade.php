@extends('layouts.adminLayout')
@section('content')


    <div class="card">

        <div class="card-body">
            <table class="table table-hover">
                <tr>
                    <th>book cover</th>
                    <th>name</th>
                    <th>Restore</th>
                    <th>Destroy</th>
                </tr>
                <tbody>
                @if($trahsed->count() >0)
                    @foreach($trahsed as $post)
                        <tr>
                            <td><img src="{{asset('uploads/'.$post->book_cover)}}" width="80px" height="80px"></td>
                            <td>{{$post->name}}</td>
                            <td><a href="{{route('books.restore',['id'=>$post->id])}}"
                                   class="btn btn-success">Restore</a></td>
                            <td>
                                <form action="{{route('books.kill',['id'=>$post->id])}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="Delete" class="btn btn-danger">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <th colspan="5" class="text-center">No Trashed Books</th>
                    </tr>
                @endif

                </tbody>
            </table>
        </div>
    </div>



@endsection