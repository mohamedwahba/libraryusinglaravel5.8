@extends('layouts.adminLayout')
@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card">
        <div class="card-header">
            Add New Book
        </div>
        <div class="card-header">
            <form action="{{route('books.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="title">Name</label>
                    <input type="text" name="title" class="form-control" value="{{old('title')}}">

                </div>
                <div class="form-group">
                    <label for="ISN">ISN</label>
                    <input type="number" value="{{old('ISN')}}" name="ISN" class="form-control">
                </div>

                <div class="form-group">
                    <label for="featured">Book cover image</label>
                    <input type="file" name="featured" class="form-control" value="{{old('featured')}}">

                </div>

                <div class="form-group">
                    <label for="author_id">Select Author</label>
                    <select name="author_id" class="form-control">
                        @foreach($authors as $auth)
                            <option value="{{$auth->id}}">{{$auth->name}}</option>
                        @endforeach

                    </select>

                </div>

                <div class="form-group">
                    <label for="category">Select Category</label>
                    <select name="category_id" class="form-control">
                        @foreach($category as $cat)
                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                        @endforeach

                    </select>

                </div>

                <div class="form-group">
                    <label for="number">Select Number of copies</label>
                    <input type="number" value="{{old('number')}}" name="number" class="form-control">
                </div>



                <div class="form-group">
                    <div class="text-center">
                        <button class="btn btn-success" type="submit">Store Book</button>
                    </div>
                </div>


            </form>
        </div>
    </div>

@endsection