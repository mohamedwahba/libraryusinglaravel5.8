@extends('layouts.adminLayout')
@section('content')


    <div class="card">

        <div class="card-body">
            <table class="table table-hover">
                <tr>
                    <th>image</th>
                    <th>name</th>
                    <th>Permissions</th>
                    <th>Delete</th>
                </tr>
                <tbody>
                @if($users->count() > 0)

                    @foreach($users as $user)
                        <tr>
                            @if($user->profile->avatar!= null)
                            <td>
                                <img src="{{asset($user->profile->avatar)}}" style="border-radius: 50%" width="80px"
                                     height="80px">
                            </td>
                            @else
                                <td>
                                    <img src="{{asset($user->profile->avatar)}}" style="border-radius: 50%" width="80px"
                                         height="80px">
                                </td>
                                @endif
                            <td>
                                {{$user->name}}
                            </td>
                            <td>
                                @if($user->role)
                                    <a href="{{route('users.user',['id'=>$user->id])}}" class="btn btn-danger">Remove permission</a>
                                @else
                                    <a href="{{route('users.admin',['id'=>$user->id])}}" class="btn btn-success">Make Admin</a>
                                @endif

                            </td>

                            <td>
                                @if(auth()->user()->id !== $user->id)
                                    <form action="{{route('users.destroy',['id'=>$user->id])}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" value="Delete" class="btn btn-danger">
                                    </form>
                                @endif
                            </td>
                        </tr>

                    @endforeach
                @else
                    <tr>
                        <th colspan="5" class="text-center">No Users</th>
                    </tr>
                @endif


                </tbody>
            </table>
        </div>
    </div>



@endsection