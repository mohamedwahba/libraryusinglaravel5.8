@extends('layouts.front')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card align-center">
                    <h1 class="text-center btn-success">All Books</h1>

                    <div class="card-body">
                    @if(session()->has('success'))
                            <p class="alert alert-success">{{session()->get('success')}}</p>
                        @endif

                        @if(session()->has('info'))
                            <p class="alert alert-danger">{{session()->get('info')}}</p>
                        @endif
                        <div class="row m-auto">
                            @foreach($books as $book)
                                <div class="card m-3 p-2" style="width: 18rem;">
                                    <img class="card-img-top" src="{{asset('uploads/'.$book->book_cover)}}"
                                         alt="Card image cap" height="200px">
                                    <div class="card-body">
                                        <h5 class="card-title">BookName: {{$book->name}}</h5>
                                        <p class="card-text">Author: {{$book->author->name}}</p>
                                    </div>
                                </div>
                            @endforeach
                        </div>





                    </div>

                  <div class="m-auto">{!! $books->links() !!}</div>

                </div>
            </div>
        </div>
    </div>
@endsection
