@extends('layouts.front')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card align-center">
                    @if(session()->has('success'))
                        <p class="alert alert-success">{{session()->get('success')}}</p>
                    @endif

                        <table class="table table-striped">
                            @if(sizeof($books) > 0)

                            <thead>
                            <tr>
                                <th scope="col">ISN</th>
                                <th scope="col">Name</th>
                                <th scope="col">Status</th>
                                <th scope="col">Return</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($books as $book)
                                <tr>
                                    <td>{{$book->ISN}}</td>
                                    <td>{{$book->name}}</td>
                                    <td class="alert alert-info">{{$row[0]->status}}</td>
                                    <td><a href="{{route('borrow.cancel',['id'=>$book->id])}}" class="btn btn-danger">Return</a>
                                    </td>
                                </tr>

                            @endforeach
                            </tbody>
                    @else
                        <tr>
                            <th colspan="5" class="text-center">No Books</th>
                        </tr>
                    @endif
                        </table>

                </div>
            </div>
        </div>
    </div>
@endsection
